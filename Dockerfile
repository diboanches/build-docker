FROM ubuntu:14.04

ENV JTITRACE_ENV=stage
ENV TZ="Europe/Moscow"
ENV JAVA_HOME=/opt/java
ENV PATH=$PATH:$JAVA_HOME/bin

copy java_jcp/java /opt/java
run mkdir -p /var/opt/cprocsp/keys/ ; mkdir -p /jtitrace/logs
copy root /var/opt/cprocsp/keys/root
run chown root -R /var/opt/cprocsp/keys/root ; chmod 700 -R /var/opt/cprocsp/keys/root

copy jti-trace-api/jti-trace-taxcom-api-stub/build/distributions/jti-trace-taxcom-api-stub-0.2-SNAPSHOT.tar /

run tar xvf /jti-trace-taxcom-api-stub-0.2-SNAPSHOT.tar -C /
entrypoint ["/jti-trace-taxcom-api-stub-0.2-SNAPSHOT/bin/jti-trace-taxcom-api-stub", ">", "/jtitrace/logs/docker-taxcom-api-stub.log", "2>&1"]
